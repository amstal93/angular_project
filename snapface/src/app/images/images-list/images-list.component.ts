import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Image } from '../../core/models/image.model';
import { ImageService } from '../../core/services/image.service';
import { interval, tap, Subject, takeUntil, Observable } from 'rxjs';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.scss']
})
export class ImagesListComponent implements OnInit, OnDestroy {

  //@Input() imagesList!: Image[];
  private destroy$!: Subject<boolean>
  imagesTab$!: Observable<Image[]>

  constructor(private imageService: ImageService) {}

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }

  ngOnInit(): void {
    //this.imagesList = this.imageService.images;
    this.imagesTab$ = this.imageService.getAllImages();

    this.destroy$ = new Subject<boolean>();
    interval(1000).pipe(
      tap(console.log),
      takeUntil(this.destroy$)
      ).subscribe();
  }

}
