import { map, tap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Observable } from 'rxjs';
import { Image } from '../../core/models/image.model';
import { ImageService } from '../../core/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-image',
  templateUrl: './new-image.component.html',
  styleUrls: ['./new-image.component.scss']
})
export class NewImageComponent implements OnInit {

  imageForm!: FormGroup;
  imagePreview$!: Observable<Image>;
  urlRegex!: RegExp;

  constructor(private formBuilder: FormBuilder, private service: ImageService, private router: Router) { }

  ngOnInit(): void {
    this.imageForm = this.formBuilder.group({
      title: [null, [Validators.required]],
      imageUrl: [null, [Validators.required, Validators.pattern(this.urlRegex)]],
      location: [null]
    },{
      updateOn: 'blur'
    })

    this.imagePreview$ = this.imageForm.valueChanges.pipe(
      map(
        formValue => ({
          ...formValue,
          createdDate: new Date,
          clicks: 0,
          id: 0
        })
      )
    )

    this.urlRegex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)/;
  }

  onSubmit(){
    console.log(this.imageForm.value);
    this.service.addImage(this.imageForm.value).pipe(
      tap(() => this.router.navigateByUrl('/images'))
    ).subscribe();
  }

}
