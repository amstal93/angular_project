import { Component, OnInit } from '@angular/core';
import { filter, interval, of, Observable } from 'rxjs';
import { concatMap, mergeMap, delay, exhaustMap, map, switchMap, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.scss']
})
export class ObservableComponent implements OnInit {

  interval$!: Observable<string>

  constructor() { }

  ngOnInit(): void {
    /* increase each seconds */
    // const interval$ = interval(1000);
    // interval$.subscribe(value => console.log(value));

    /* increase each seconds but start after 3 seconds */
    // setTimeout(() => {
    //   interval$.subscribe(value => console.log(value))
    // }, 3000);

    this.interval$ = interval(1000).pipe(
      filter(value => value % 3 === 0),
      map(value => value % 2 === 0 ?
        `Je suis ${value} et je suis pair` :
        `Je suis ${value} et je suis impair`
      ),
      tap(text => console.log(text))
    );
  }

}
