import { map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Image } from '../models/image.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ImageService {

  images: Image[] = [];

  constructor(private http: HttpClient){}

  getAllImages(): Observable<Image[]> {
    return this.http.get<Image[]>('http://localhost:3000/images');
  }

  getImage(id:number): Observable<Image>{
    return this.http.get<Image>(`http://localhost:3000/images/${id}`);
  }

  addImage(formValue: {title: string, imageUrl: string, location?: string}): Observable<Image>{

    /* const image: Image = {
      ...formValue,
      clicks: 0,
      createdDate: new Date,
      id: this.images[this.images.length - 1].id + 1
    }; */
    return this.getAllImages().pipe(
      map(images => [...images].sort((a,b) => a.id - b.id)),
      map(sortedImages => sortedImages[sortedImages.length -1]),
      map(previousImage => ({
        ...formValue,
        clicks: 0,
        createdDate: new Date(),
        id: previousImage.id + 1
      })),
      switchMap(newImage => this.http.post<Image>(
        'http://localhost:3000/images',
        newImage
      ))
    )

  }

  onClick(id:number): Observable<Image> {
    return this.getImage(id).pipe(
      map(image => ({
        ...image,
        clicks: image.clicks + 1
      })),
      switchMap(updatedImage => this.http.put<Image>(
        `http://localhost:3000/images/${id}`,
        updatedImage
      ))
    )
  }


}
